export default {
    actions: {},
    mutations: {
        addUser(state, obj) {
            if (obj.age < 25) {
                state.teens.push(obj)
            }
            if (obj.age >= 25 && obj.age < 50) {
                state.middleAged.push(obj)
            } 
            if(obj.age >= 50){
                state.old.push(obj)
            }
        }
    },
    state: {
        teens: [{
                name: 'Selena',
                age: 19
            },
            {
                name: 'Karina',
                age: 18
            },
            {
                name: 'Shirin',
                age: 16
            },
        ],
        middleAged: [{
                name: 'Melisa',
                age: 43
            },
            
        ],
        old: [
            {
                name: 'Vasiliy',
                age: 67
            },
            {
                name: 'Vasiliy',
                age: 78
            },
        ]
    },
    getters: {
        allUsers(state) {
            return state.teens
        },
        middleAged(state) {
            return state.middleAged
        },
        old(state){
            return state.old
        }


    }
}